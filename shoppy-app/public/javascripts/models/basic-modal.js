/**
 * Created by I313712 on 13/01/2015.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'router', // Request router.js
    'sticker'
], function($, _, Backbone, Router,Sticker){
    var basicModal =new Backbone.Model({title: 'Example Modal', body: 'Hello World'});

    return basicModal;
});
