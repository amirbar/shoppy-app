/**
 * Created by I313712 on 13/01/2015.
 */

require.config({

   paths: {

       jquery: 'libs/jquery-1.11.2',
       underscore: 'libs/underscore',
       backbone: 'libs/backbone',
       bootstrap: 'libs/bootstrap.min',
       sticker: 'libs/sticker.min',
       text: 'libs/text'

   }




});

require([

    'app',

], function(App){

   App.initialize();

});