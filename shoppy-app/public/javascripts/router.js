/**
 * Created by I313712 on 13/01/2015.
 */
// Filename: router.js
define([
    'jquery',
    'underscore',
    'backbone',
    'views/basic-modal',
    'text!../templates/basic-modal.html'
], function($, _, Backbone, BasicModal, ModalTemplate){
    var AppRouter = Backbone.Router.extend({
        routes: {
            "crawl/:id":"onCrawl",
            "*actions": "defaultRoute" // matches http://example.com/#anything-here
        }
    });
    var initialize = function() {
        var app_router = new AppRouter;

        app_router.on('route:onCrawl', function (id) {
            var basicModal = new BasicModal();
            basicModal.render();

            console.log("crawl "+id);

        });
        app_router.on('route:defaultRoute', function () {
            console.log("default");
        });

        // Start Backbone history a necessary step for bookmarkable URL's
        Backbone.history.start();
    }
    return {
        initialize: initialize
    };
});