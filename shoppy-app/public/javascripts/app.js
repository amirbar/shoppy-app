/**
 * Created by I313712 on 13/01/2015.
 */


define([
    'jquery',
    'underscore',
    'backbone',
    'router', // Request router.js
    'sticker'
], function($, _, Backbone, Router,Sticker){
    var initialize = function(){
        // Pass in our Router module and call it's initialize function
        Router.initialize();
        Sticker.init('.sticker');
    }

    return {
        initialize: initialize
    };
});
