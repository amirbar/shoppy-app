/**
 * Created by I313712 on 13/01/2015.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'text!../../templates/basic-modal.html',
    'bootstrap'



], function($, _, Backbone, modalTemplate){

    var ModalView = Backbone.View.extend({

        el: $('#modal-template'),
        events: {
            'click .close': 'close'
        },
        render: function () {
            var compiled = _.template(modalTemplate);
            this.$el.html(compiled({title:'whoooo',
                                    body: 'gofasasdasdasdasd'}));
            this.show();

        },

        show: function () {
            this.$el.find('#myModal').modal('show');
        },

        close: function () {
            this.$el.find('#myModal').modal('hide');
        }
    });

    return ModalView;


});

