/**
 * Created by Amir on 20/02/2015.
 */
var mongoose = require('mongoose');

var ProductController = function(app){
    var ProductSchema = require('../models/Product');
    var Product = mongoose.model('Product',ProductSchema);

    /*
        GET ALL PRODUCTS
     */

    app.get('/product/all',function(req,res,next){
        Product.find({},function(err,val){
            if (err)
                res.send(err);
            else
                res.send(val);
        });

    });


    /*
        GET A SINGLE PRODUCT
     */

    app.get('/product/:id', function(req, res) {
        var productId = req.params.id;
        try{


            Product.find({"productId": productId}, function(err,productInfo){

                if (err){

                    res.send(err);

                }else{
                    res.send(productInfo);
                }

            });

        }catch(e){
            console.log(e);
        }
    });


}

module.exports =  ProductController;