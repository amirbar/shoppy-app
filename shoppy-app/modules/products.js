/**
 * Created by Amir on 19/02/2015.
 */
var Product = require('../models/Product.js');
var products = {};

//init for test purposes

var first = new Product(1,"pasta",10);
var second = new Product(2,"rice",12);
var third = new Product(3,"corn",17);

var arr = [];
arr.push(first);
arr.push(second);
arr.push(third);


products.getProductById = function(productId){

    for (var i=0; i<arr.length; i++){
        if (arr[i].id == productId)
            return arr[i];
    }
    return {};
};
products.getAllProducts = function(){
    return arr;


};
module.exports = products;