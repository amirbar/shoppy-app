var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cheerio = require('cheerio');
var request = require('request');
var iconv  = require('iconv-lite');
var routes = require('./routes/index');
var users = require('./routes/users');
var jschardet = require('jschardet');
var charset = require('charset');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
/*app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));*/
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/scrape',function(req,res){

    var url = "http://www.rami-levy.co.il/?catid={C4A440EC-4017-4B98-B218-5B523B68992C}";
    var requestOptions  = { encoding:null,uri: url};
    request(requestOptions,function(error,response,html){

        if (!error){
            enc =charset(response.headers,html);
            enc = enc || jschardet.detect(html).encoding.toLowerCase();

            var str = iconv.decode(new Buffer(html),enc);

            var $ = cheerio.load(str);
            var arr = [];
            var products = $('.prodDescTD');
            var productNames = $('.prodName');
            var prodPrice = $('.prodPrice');

            for (var i=0; i<products.length; i++)
            {
                var jsonElem = {};
                jsonElem.productId = products[i].attribs.id;
                jsonElem.name = $(productNames[i]).text();
                var price = $(prodPrice[i]).text();
                jsonElem.price = price.split(' ')[0];

                arr.push(jsonElem);
            }

            console.log(arr);

        }

    });
});



// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});

